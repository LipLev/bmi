package BMICalculator.BMIC;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import BMI.Type;

/**
 * @author Lippai Levente
 *
 */
public class TypeTest {

  private Type typetest;
  
  @Before
  public void setUp() throws Exception {
    typetest = new Type();
  }
  
  
  //  Kategóriák unit tesztelése
  
  @Test
  public void testType1 () {
      Assert.assertSame("Severe Thiness", typetest.whichCategory(10));
  }
  
  public void testType2 () {
      Assert.assertSame("Moderate Thinness", typetest.whichCategory(16));
  }
  
  @Test
  public void testType3 () {
      Assert.assertSame("Mild Thinness", typetest.whichCategory(17));
  }
  
  @Test
  public void testType4 () {
      Assert.assertSame("Normal", typetest.whichCategory(20));
  }
  
  @Test
  public void testType5 () {
      Assert.assertSame("Overweight", typetest.whichCategory(25));
  }
  
  @Test
  public void testType6 () {
      Assert.assertSame("Obese Class I", typetest.whichCategory(32));
  }
  
  @Test
  public void testType7 () {
      Assert.assertSame("Obese Class II", typetest.whichCategory(37));
  }
  
  @Test
  public void testType8 () {
      Assert.assertSame("Obese Class III", typetest.whichCategory(200));
  }
  
  
}
