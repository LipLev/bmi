package BMICalculator.BMIC;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import BMI.BMIcalc;

/**
 * @author Lippai Levente
 *
 */
public class BMICalcTest {

  private BMIcalc bmic;

  @Before
  public void setUp() throws Exception {
    bmic = new BMIcalc();
  }

  @Test
  public void testBMICalc() {

    // When
    double bmiCalcResult = bmic.bmi(1.8, 60);

    // Then
    Assert.assertEquals(18.52, bmiCalcResult, 0.01);
  }
}
