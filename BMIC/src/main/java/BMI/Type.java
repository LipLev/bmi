package BMI;

/**
 * @author Lippai Levente
 *
 */

public class Type {
  
 /** A kiszámított testtömegindex alapján, megmutatja hogy melyik kategóriába tartozik a kapott érték.
  * 
   * @param calcResult
   * @return String
   */
  public String whichCategory(double calcResult) {
    
    if (calcResult < 16) {
      return "Severe Thiness";
    }
    else if (16 <= calcResult && calcResult < 17) {
      return "Moderate Thinness";
    }
    else if (17 <= calcResult && calcResult < 18.5) {
      return "Mild Thinness";
    }
    else if (18.5 <= calcResult && calcResult < 25) {
      return "Normal";
    }
    else if (25 <= calcResult && calcResult < 30) {
      return "Overweight";
    }
    else if (30 <= calcResult && calcResult < 35) {
      return "Obese Class I";
    }
    else if (35 <= calcResult && calcResult < 40) {
      return "Obese Class II";
    }
    else {
      return "Obese Class III";
    }
  }
}
