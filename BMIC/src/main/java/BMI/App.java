package BMI;

/**
 * @author Lippai Levente
 *
 */

public class App {

  public static void main(String[] args) {

    BMIcalc nextCalc = new BMIcalc();

    Double calcResult = nextCalc.bmi(1.82, 82);

    //A testtömegindex számítás során kapott double érték rövidítése 2 tizedesjegyig a jobb átláthatóság miatt.
    double modCalcResult = (double) Math.round(calcResult * 100) / 100;

    //Hibát jelez, amennyiben negatív szám kerül megadásra a magasság vagy a súly paramétereként.
    if (calcResult > 0) {
      Type type = new Type();

      String category = type.whichCategory(calcResult);
      
      System.out.println(
          "Your BMI is " + modCalcResult + ", so you are in the " + category + " category.");

    } else {
      System.out.println("Use positive numbers as a parameter!");

    }
  }
}
