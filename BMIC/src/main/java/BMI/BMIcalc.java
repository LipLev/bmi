package BMI;

/**
 * @author Lippai Levente
 *
 */
public class BMIcalc {

  /** Testtömegindex kiszámítása a magasság és a súly alapján.
   * @param height : méter, double
   * @param weight : kilogramm, double
   * @return double
   */
  public double bmi(double height, double weight) {
    return weight / (height * height);
  }
}